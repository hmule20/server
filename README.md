# server

**Django Start Project Boiler Plate**

`Which Includes:`
* Postgres Database Configuration
* JWT, JWE token configuration
* requirements.txt
* user app with User Model and it's migrations
* .gitignore


**Steps to install**:
*  git clone git@gitlab.com:hmule20/server.git
*  pip install virtualenv venv_name
*  Activate the environment:
    *  for (linux): source venv_name\bin\activate
    *  for (windows): venv_name\Scripts\activate
*  cd generalized-django-server
*  pip install -r requirements.txt
*  python manage.py migrate

***Note***: Add a .env file